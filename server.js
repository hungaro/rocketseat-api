const express = require('express'); //O express server para criar rotas e acesso as view e este comando esta importando ele
const mongoose = require('mongoose');

//Iniciando o app
const app = express(); //Executa a função express


//Iniciando o banco de dados
mongoose.connect('mongodb://localhost:27017/nodeApi', {useNewUrlParser: true});

//Primeira rota
app.get('/', (req, res) => { //req faz uma requisicao para o servidor e o  resé a resposta para o usuario
    res.send('Hello Vinicius');
})

app.listen(3001);//Serve para escutar a porta 

